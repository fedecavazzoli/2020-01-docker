#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./start.sh <PORT>"
	exit 1
fi

TEST_PORT=$1

docker build . -t file-server
docker run -p $TEST_PORT:80 -d file-server
