#!/bin/sh

docker build . -t layers
docker run layers
docker history layers >> history.txt
docker save layers > layer.tar
